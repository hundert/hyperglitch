﻿using HG.Controllers;
using System.Collections.Generic;
using UnityEngine;
using static HG.Utils.InputManager;

namespace HG.Battle
{

    public class ControllerManager : MonoBehaviour
    {
        [SerializeField]
        private PlayerCharacterController _p1CharacterController = null;
        public PlayerCharacterController P1CharacterController => _p1CharacterController;

        [SerializeField]
        private PlayerCharacterController _p2CharacterController = null;
        public PlayerCharacterController P2CharacterController => _p2CharacterController;


        private void FixedUpdate()
        {
            List<InDirection> directions = new List<InDirection>();
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                directions.Add(InDirection.Left);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                directions.Add(InDirection.Right);
            }
            var dir = ResolveDirection(directions.ToArray());
            _p1CharacterController.FeedInput(dir, Button.None);

            _p1CharacterController.Increment();
        }
    }
}
