﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HG.Controllers
{
    public class GameStateController : MonoBehaviour
    {
       public enum GameState
        {
            Playing,
            Hitstop,
            Pause,
            BeforeMatch,
            AfterMatch
        }

        [SerializeField]
        private GameState _currentState = GameState.BeforeMatch;
        public GameState CurrentState { get => _currentState; }

        private void FixedUpdate()
        {
            switch(_currentState)
            {
                case GameState.BeforeMatch:
                case GameState.Hitstop:
                    //Fill input buffers
                    break;
                case GameState.Playing:
                    //Fill input buffers
                    //Increment
                    break;
            }
        }

    }
}