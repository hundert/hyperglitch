﻿using HG.Battle;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HG.Controllers
{
    public class ActionPartController : MonoBehaviour
    {

        [SerializeField]
        private string _id = "";
        public string ID { get => _id; }

        private PlayerCharacterController _controller = null;
        private List<ActionPart> _actionParts;

        private ActionPart _currentPart = null;
        public ActionPart CurrentPart { get => _currentPart; }

        private int _currentFrame = 0;
        public int CurrentFrame { get => _currentFrame; }


        internal void InitializeAction(PlayerCharacterController controller)
        {
            _controller = controller;
            _actionParts = GetComponentsInChildren<ActionPart>().ToList();
            foreach (ActionPart part in _actionParts)
            {
                part.Initialize(this);
            }

        }

        internal void IncrementAction()
        {
            _currentFrame += 1;

        }

        internal void SetSprite(Sprite sprite)
        {
            _controller.SetSprite(sprite);
        }
    }
}
