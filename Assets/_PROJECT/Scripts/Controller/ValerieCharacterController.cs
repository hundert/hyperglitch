﻿using HG.Utils;

namespace HG.Controllers
{
    public class ValerieCharacterController : PlayerCharacterController
    {
        public override void Increment()
        {
            if (CurrentState == CharacterState.Neutral)
            {
                var dir = directionInputBuffer.Pop();
                if (dir == InputManager.Direction.Neutral)
                {
                    SetAnimator(AnimatorState.Idle);
                }
                else
                {
                    SetAnimator(AnimatorState.Walk);
                    Walk(dir);
                }
            }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
