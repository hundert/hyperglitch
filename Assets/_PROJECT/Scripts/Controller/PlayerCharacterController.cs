﻿using HG.Data;
using System.Collections.Generic;
using UnityEngine;
using static HG.Utils.InputManager;

namespace HG.Controllers
{
    public abstract class PlayerCharacterController : MonoBehaviour
    {
        #region general character params
        public enum CharacterState
        {
            Neutral,
            Attacking,
            Hitstun,
            Parry,
            ParryRecovery
        }

        protected enum AnimatorState
        {
            Idle,
            Walk,
            Jump,
            Hit,
            None
        }

        [Header("Character State")]
        [SerializeField]
        protected CharacterState currentState = CharacterState.Neutral;
        public CharacterState CurrentState { get => currentState; }

        [SerializeField]
        protected bool facingRight = true;
        public bool FacingRight { get => facingRight; }

        [SerializeField]
        protected int health = 10;
        public int Health { get => health; }

        [SerializeField]
        protected int maxHealth = 10;
        public int MaxHealth { get => maxHealth; }

        [SerializeField]
        protected bool airbourne = true;
        public bool Airbourne { get => airbourne; }

        [SerializeField]
        protected MovementStats movement = new MovementStats();
        public MovementStats Movement { get => movement; }
        #endregion

        #region input buffer
        protected Stack<Direction> directionInputBuffer = new Stack<Direction>(5);
        protected Stack<Button> buttonInputBuffer = new Stack<Button>(5);

        public virtual void InitializeInputBuffer(int bufferSize)
        {
            InitializeInputBuffer(bufferSize, bufferSize);
        }
        public virtual void InitializeInputBuffer(int buttonBufferSize, int directionBufferSize)
        {
            directionInputBuffer = new Stack<Direction>(directionBufferSize);
            buttonInputBuffer = new Stack<Button>(buttonBufferSize);
        }

        public virtual void FeedInput(Direction direction, Button button)
        {
            directionInputBuffer.Push(direction);
            buttonInputBuffer.Push(button);
        }
        #endregion

        #region linked objects
        [Header("Linked Objects")]
        [SerializeField]
        private Transform _actionTreeRoot = null;

        [SerializeField]
        private SpriteRenderer _characterSprite = null;

        [SerializeField]
        private Animator _characterAnimator = null;
        #endregion

        #region private variables
        private AnimatorState _lastAnimatorState = AnimatorState.Idle;
        private int _remainingJumps = 2;
        private float _jumpHorizontalVelocity = 0f, _jumpVerticalVelocity = 0f;
        #endregion

        public abstract void Increment();

        public void SetSprite(Sprite newSprite)
        {
            _characterSprite.sprite = newSprite;
        }

        #region common behaviours


        protected void SetAnimator(AnimatorState newState)
        {
            if ((newState == AnimatorState.Idle) || (newState == AnimatorState.Walk))
            {
                if (newState == _lastAnimatorState) return;
            }

            _lastAnimatorState = newState;
            _characterAnimator.enabled = newState != AnimatorState.None;

            switch (newState)
            {
                case AnimatorState.Idle:
                    _characterAnimator.SetTrigger("idle");
                    break;
                case AnimatorState.Walk:
                    _characterAnimator.SetTrigger("walk");
                    break;
                case AnimatorState.Jump:
                    _characterAnimator.SetTrigger("Jump");
                    break;
                case AnimatorState.Hit:
                    _characterAnimator.SetTrigger("hit");
                    break;
            }

        }

        private float GetHorizontalMomentum(Direction dir)
        {
            float speed = 0;
            if (dir == Direction.Forward)
            {
                speed = facingRight ? movement.forwardWalkspeed : (movement.forwardWalkspeed * -1);
            }
            else if (dir == Direction.Back)
            {
                speed = facingRight ? (movement.backWalkspeed * -1) : movement.backWalkspeed;
            }
            return speed;
        }

        protected virtual void Walk(Direction dir)
        {
            transform.parent.Translate(Vector3.right * GetHorizontalMomentum(dir));
        }

        protected virtual void Jump(Direction dir)
        {
            if (_remainingJumps < 1) return;

            _remainingJumps -= 1;
            airbourne = true;

            _jumpVerticalVelocity = movement.jumpVelocity;
            if (dir == Direction.UpForward)
            {
                _jumpHorizontalVelocity = GetHorizontalMomentum(Direction.Forward);
            }
            else if (dir == Direction.UpBack)
            {
                _jumpHorizontalVelocity = GetHorizontalMomentum(Direction.Back);
            }
            else
            {
                _jumpHorizontalVelocity = 0f;
            }
            ProcessJump();
        }

        private void ProcessJump()
        {
            transform.Translate(new Vector3(_jumpHorizontalVelocity, _jumpVerticalVelocity, 0));
            _jumpVerticalVelocity -= movement.gravity;
        }
        #endregion
    }
}
