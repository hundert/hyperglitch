﻿using HG.Controllers;
using HG.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace HG.Battle
{
    public class ActionPart : MonoBehaviour
    {
        #region move base params
        [SerializeField]
        private int _firstFrame = 1;
        public int FirstFrame { get => _firstFrame; }

        [SerializeField]
        private int _lastFrame = 2;
        public int LastFrame { get => _lastFrame; }

        [SerializeField]
        private bool _overrideDefaultHurtbox = false;
        public bool OverrideDefaultHurtbox { get => _overrideDefaultHurtbox; }
        #endregion

        [SerializeField]
        private List<Sprite> _frames = new List<Sprite>();

        private ActionPartController _controller = null;
        private bool _initialized = false;

        internal void Initialize(ActionPartController controller)
        {
            _controller = controller;
        }

        internal void ActivateHitboxes()
        {

        }

        internal void DeactivateHitboxes()
        {

        }

        internal void Increment(int frameCount)
        {
            if (!_initialized) throw new NotInitializedException();

            int currentFrame = frameCount - _firstFrame;

            if (currentFrame < _frames.Count)
            {
                _controller.SetSprite(_frames[currentFrame]);
            }
            else
            {
                _controller.SetSprite(_frames[_frames.Count - 1]);
            }
        }
    }
}
