﻿using UnityEngine;

namespace HG.Utils
{
    public class InputManager : MonoBehaviour
    {
        #region inputs
        public enum InDirection
        {
            Up,
            Down,
            Left,
            Right
        }
        public enum InButton
        {
            A = 1,
            B = 2,
            C = 3,
            D = 4
        }
        #endregion
        #region outputs
        public enum Button
        {
            None = 0,
            A = 1,
            B = 2,
            C = 3,
            D = 4,
            ABC = 5
        }
        public enum Direction
        {
            DownBack = 1,
            Down = 2,
            DownForward = 3,
            Back = 4,
            Neutral = 5,
            Forward = 6,
            UpBack = 7,
            Up = 8,
            UpForward = 9
        }
        #endregion
        #region resolvers
        public static Direction ResolveDirection(InDirection[] inputs, bool facingRight = true)
        {
            bool left = false, right = false, up = false;

            foreach (InDirection dir in inputs)
            {
                if (dir == InDirection.Left)
                {
                    left = true;
                }
                else if (dir == InDirection.Right)
                {
                    right = true;
                }
                else if (dir == InDirection.Up)
                {
                    up = true;
                }
            }

            if (up)
            {
                if (left && !right)
                {
                    return facingRight ? Direction.UpBack : Direction.UpForward;
                }
                else if (right && !left)
                {
                    return facingRight ? Direction.UpForward : Direction.UpBack;
                }
                else return Direction.Up;
            }
            else
            {
                if (left && !right)
                {
                    return facingRight ? Direction.Back : Direction.Forward;
                }
                else if (right && !left)
                {
                    return facingRight ? Direction.Forward : Direction.Back;
                }
                else return Direction.Neutral;
            }
        }

        public static Button ResolveButton(InButton[] buttons)
        {
            Button output = Button.None;
            foreach (InButton b in buttons)
            {
                if ((int)b > (int)output)
                {
                    output = (Button)b;
                }
            }
            return output;
        }
        #endregion
    }
}
