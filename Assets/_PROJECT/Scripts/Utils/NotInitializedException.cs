﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HG.Utils
{
    public class NotInitializedException : Exception
    {
        public NotInitializedException()
        {
        }

        public NotInitializedException(string message)
            : base(message)
        {
        }

        public NotInitializedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}