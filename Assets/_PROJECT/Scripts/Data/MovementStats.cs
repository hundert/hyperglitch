﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HG.Data
{
    [System.Serializable]
    public class MovementStats
    {
        public float forwardWalkspeed = 0.5f;

        public float backWalkspeed = 0.5f;

        public float jumpVelocity = 2.5f;

        public float gravity = 0.05f;

        public int maxJumps = 2;
    }
}
